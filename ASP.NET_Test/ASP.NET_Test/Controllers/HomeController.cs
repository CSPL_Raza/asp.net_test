﻿using ASP.NET_Test.Attributes;
using ASP.NET_Test.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_Test.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult commonErrorAction()
        {
            try
            {
                string description = TempData["ErrorMessage"] as string;
                string path = System.Web.HttpContext.Current.Server.MapPath("~\\LogFile");
                string strFile = path + @"\error" + DateTime.Now.Date.ToString("ddMMyyyy") + ".txt";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var objStreamWriter = new StreamWriter(strFile, true);
                objStreamWriter.WriteLine("Error Message : " + description + "  Date:" + DateTime.Now);
                objStreamWriter.Dispose();

            }
            catch (Exception ex)
            {
            }
            return View();
        }
        // GET: Home
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }

        }

        [HttpPost]
        public ActionResult Index(string username, string password)
        {
            try
            {
                TestProjectEntities db = new TestProjectEntities();

                user loggeduser = db.users.Where(temp => temp.uname == username && temp.psk == password).FirstOrDefault();
                if (loggeduser != null)
                {
                    Session["IsLoggedIn"] = true;
                    // Session["Role"] = "User";

                    return RedirectToAction("Details", new { uid = loggeduser.uid });
                }
                else
                {
                    adminList loggedadmin = db.adminLists.Where(temp => temp.username == username && temp.password == password).FirstOrDefault();
                    if (loggedadmin != null)
                    {
                        Session["IsLoggedIn"] = true;
                        //     Session["Role"] = "Admin";
                        return RedirectToAction("DetailsAdmin");
                    }
                    else
                    {
                        return RedirectToAction("LoginErrorPage");
                    }

                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }


        }
        public ActionResult Register()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }

        }

        public ActionResult LoginErrorPage()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }

        }

        public ActionResult RegisterErrorPage()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }

        }

        [HttpPost]
        public ActionResult Register(user newUserInfo)
        {
            try
            {
                string errorMessage = "";
                TestProjectEntities db = new TestProjectEntities();
                user matchedDataUser = db.users.Where(temp => temp.MNo == newUserInfo.MNo || temp.mail == newUserInfo.mail).FirstOrDefault();
                if (matchedDataUser != null)
                {
                    errorMessage = "A user with the same mobile number or email already exists.";

                }
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    ViewBag.errorMessage = errorMessage;
                    ViewBag.user = newUserInfo;
                    return View("Register");
                }
                db.users.Add(newUserInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");
            }
        }

        [SessionCheck]
        public ActionResult Details(int uid)
        {
            try
            {
                TestProjectEntities db = new TestProjectEntities();
                user loggeduser = db.users.Where(temp => temp.uid == uid).FirstOrDefault();
                return View(loggeduser);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }

        }

        [SessionCheck]
        public ActionResult DetailsAdmin()
        {
            try
            {
                TestProjectEntities db = new TestProjectEntities();
                List<user> allusers = db.users.ToList();
                return View(allusers);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }
          ;
        }


        [HttpPost]
        public ActionResult Edit(int uid)
        {
            try
            {
                TestProjectEntities db = new TestProjectEntities();
                user loggeduser = db.users.Where(temp => temp.uid == uid).FirstOrDefault();
                return View(loggeduser);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }

        }

        [HttpPost]
        public ActionResult Update(user user, HttpPostedFileBase file)
        {
            try
            {
                TestProjectEntities db = new TestProjectEntities();
                string errorMessage = "";
                if (file != null && file.ContentLength > 0)
                {
                    string fileName = System.IO.Path.GetFileName(file.FileName);
                    string filePath = "~/profilePhotos/" + fileName;
                    file.SaveAs(Server.MapPath(filePath));
                    user.profilephoto = filePath;
                }
                user existingUser = db.users.Where(temp => temp.uid == user.uid).FirstOrDefault();

                user matchedDataUser = db.users.Where(temp => temp.MNo == user.MNo || temp.mail == user.mail).FirstOrDefault();

                if (matchedDataUser != null)
                {
                    errorMessage = "A user with the same mobile number or email already exists.";

                }
                existingUser.uname = user.uname;
                if (user.DOB != null)
                {
                    existingUser.DOB = user.DOB;
                }
                existingUser.MNo = user.MNo;
                existingUser.mail = user.mail;
                if (user.profilephoto != null)
                {
                    existingUser.profilephoto = user.profilephoto;
                }
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    ViewBag.errorMessage = errorMessage;
                    return View("Edit", user);
                }
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Details", new { uid = user.uid });
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    ViewBag.errorMessage = errorMessage;
                    return View("Edit", user);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }
        }
        public ActionResult Logout()
        {
            try
            {
                Session["IsLoggedIn"] = null;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message + ex.InnerException + ex.StackTrace;
                return RedirectToAction("commonErrorAction");

            }

        }
    }
}