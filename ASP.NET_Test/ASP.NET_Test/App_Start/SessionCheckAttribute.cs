﻿using System;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_Test.Attributes
{
    public class SessionCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            if (session["IsLoggedIn"] == null || !(bool)session["IsLoggedIn"])
            {
                filterContext.Result = new RedirectResult("~/home/index");
                base.OnActionExecuting(filterContext);
            }
        }
    }
}
